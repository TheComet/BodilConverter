/*
 * Copyright 2014 Goblom.
 */
package com.bodilgames.bodilconverter.conversion;

public enum RankOld {
    OWNER(Rank.OWNER),
    DEVELOPER(Rank.DEV),
    ADMIN(Rank.ADMIN),
    MODERATOR(Rank.MOD),
    HELPER(Rank.HELPER),
    FRIEND(Rank.FRIEND),
    BUILDER(Rank.BUILDER),
    YOUTUBE(Rank.YOUTUBER),
    ULTRA(Rank.ULTRA),
    VIP_PLUS(Rank.VIP),
    VIP(Rank.VIP),
    MEMBER(Rank.DEFAULT);

    private Rank newRank;

    RankOld(Rank rank) {
        this.newRank = rank;
    }

    public static RankOld getRank(String string) {
        for (RankOld rank : RankOld.values()) {
            if (string.equalsIgnoreCase(rank.name())) {
                return rank;
            }
        }

        return MEMBER;
    }

    public Rank getNewRank() {
        return newRank;
    }
}
