package com.bodilgames.bodilconverter.conversion;

public enum Rank {
    DEFAULT,
    VIP,
    ULTRA,
    YOUTUBER,
    BUILDER,
    FRIEND,
    HELPER,
    MOD,
    ADMIN,
    DEV,
    OWNER;
}
