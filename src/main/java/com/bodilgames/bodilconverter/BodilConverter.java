package com.bodilgames.bodilconverter;

import com.bodilgames.bodilconverter.config.HistoryConfig;
import com.bodilgames.bodilconverter.config.JsonConfig;
import com.bodilgames.bodilconverter.config.ResultConfig;
import com.bodilgames.bodilconverter.conversion.RankOld;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class BodilConverter {
    private static HistoryConfig historyConfig;
    private static ResultConfig resultConfig;

    public static void main(String[] args) {
        historyConfig = JsonConfig.load(new File("./history.json"), HistoryConfig.class);
        File file = new File("./results.json");
        int i = 1;
        while (file.exists()) {
            file = new File("./results.json" + i);
            i += 1;
        }
        resultConfig = JsonConfig.load(file, ResultConfig.class);

        for (String name : historyConfig.history.keySet()) {
            UUID uuid;
            try {
                uuid = retrieve(name);
            } catch (Exception e) {
                System.out.println("Failed to check username: " + name);
                e.printStackTrace();
                continue;
            }

            if (uuid != null) {
                resultConfig.results.put(uuid.toString(), RankOld.getRank(historyConfig.history.get(name)).getNewRank().name());
            }
        }

        resultConfig.save(file);
    }

    public static UUID retrieve(String name) throws Exception {
        String sURL = "https://api.mojang.com/users/profiles/minecraft/%s?at=1422748800";
        sURL = String.format(sURL, name);

        // Connect to the URL using java's native library
        URL url = new URL(sURL);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        // Convert to a JSON object to print data
        JSONParser parser = new JSONParser();
        JSONObject object = (JSONObject) parser.parse(new InputStreamReader(request.getInputStream()));

        UUID uuid = null;
        if (object.get("id") != null) {
            String id = (String) object.get("id");
            uuid = getUUID(id);
        }
        return uuid;
    }

    private static UUID getUUID(String id) {
        return UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" + id.substring(20, 32));
    }
}
