package com.bodilgames.bodilconverter.config;

import java.util.HashMap;
import java.util.Map;

public class HistoryConfig extends JsonConfig {
    public Map<String, String> history = new HashMap<String, String>(){{ put("Flavorlock", "DEFAULT"); put("Favorlock", "DEVELOPER"); }};
}
