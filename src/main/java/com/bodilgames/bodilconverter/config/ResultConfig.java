package com.bodilgames.bodilconverter.config;

import java.util.HashMap;
import java.util.Map;

public class ResultConfig extends JsonConfig {
    public Map<String, String> results = new HashMap<>();
}
